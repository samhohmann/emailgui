/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIFrame;
import java.io.File;
import java.util.Scanner;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
/*
 *
 * Test parser. I still can't seem to get the email extractor to work on my computer, so
 * I'm having it parse text files for now.
 * Here is how the parser currently assumes the text file is laid out:
 * 
 * To:<recipient1>,<recipient2>,...,<recipientN>
 * From:<sender1>,<sender2>,...,<senderN>
 * Subject:<subject>
 * Date:mm/dd/yyyy
 * <message content>
 * ***
 * <next message>
 */
public class FileParser {
    
    public static ArrayList<Email> parse(File f)
    {
        File input = f;
        Scanner parser = null;
        ArrayList<Email> output = new ArrayList<Email>();
        ArrayList<String> tos = new ArrayList<String>();
        HashSet<String> froms = new HashSet<String>();
        String subj = "";
        MyDate date = new MyDate(1, 1, 2015);
        StringBuffer body = new StringBuffer();
        try{
            parser = new Scanner(input);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        Scanner lineParser = null;
        while(parser.hasNextLine())
        {
            lineParser = new Scanner(parser.nextLine());
            String infoType = lineParser.next();
            if(infoType.equals("To:"))
            {
                lineParser.useDelimiter(",");
                while(lineParser.hasNext())
                {
                    tos.add(lineParser.next());
                }
            }
            else if(infoType.equals("From:"))
            {
                lineParser.useDelimiter(",");
                while(lineParser.hasNext())
                {
                    froms.add(lineParser.next());
                }
            }
            else if(infoType.equals("Subject:"))
            {
                subj = lineParser.nextLine();
                subj = subj.trim();
            }
            else if(infoType.equals("Date:"))
            {
                String dateString = lineParser.next();
                dateString.trim();
                Scanner dateParser = new Scanner(dateString);
                dateParser.useDelimiter("/");
                date.setMonth(dateParser.nextInt());
                date.setDay(dateParser.nextInt());
                date.setYear(dateParser.nextInt());
            }
            else if(infoType.equals("***"))
            {
                output.add(new Email(tos, froms, date, subj, body));
                tos = new ArrayList<String>();
                froms = new HashSet<String>();
                body = new StringBuffer();
            }
            else
            {
                body.append(lineParser.nextLine());
            }
            lineParser.reset();
            
        }
        
        return output;
        
        
        
    }
            
}
//
