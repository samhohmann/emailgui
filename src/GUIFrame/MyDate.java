/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIFrame;

/**
 *
 * @author samanthahohmann
 */
public class MyDate {
    private int month;
    private int day;
    private int year;
    
    public MyDate(int m, int d, int y)
    {
        month = m;
        day = d;
        year = y;
    }
    
    public int getMonth()
    {
        return month;
    }
    
    public int getDay()
    {
        return day;
    }
    
    public int getYear()
    {
        return year;
    }
    
    public void setMonth(int m)
    {
        month = m;
    }
    
    public void setDay(int d)
    {
        day = d;
    }
    
    public void setYear(int y)
    {
        year = y;
    }
    
    public String toString()
    {
        return month + "/" + day + "/" + year;
    }
    
}
