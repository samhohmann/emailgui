/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIFrame;

import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author samanthahohmann
 */
public class Email {
    private ArrayList<String> recipients;
    private HashSet<String> senders;
    private MyDate sentDate;
    private String subject;
    private StringBuffer content;
    
    public Email(ArrayList<String> to, HashSet<String> from, MyDate d,
            String subj, StringBuffer body)
    {
        recipients = to;
        senders = from;
        sentDate = d;
        subject = subj;
        content = body;
    }
    
    public ArrayList<String> getRecipients()
    {
        return recipients;
    }
    
    public HashSet<String> getSenders()
    {
        return senders;
    }
    
    public MyDate getDate()
    {
        return sentDate;
    }
    
    public String getSubject()
    {
        return subject;
    }
    
    public StringBuffer getContent()
    {
        return content;
    }
}
