
Address[] in = message.getFrom();
for (Address address : in) {
    System.out.println("FROM:" + address.toString());
}

Multipart mp = (Multipart) message.getContent();

System.out.println("SENT DATE:" + message.getSentDate());
System.out.println("SUBJECT:" + message.getSubject());


Object msgContent = message.getContent();

String content = "";             

 /* Check if content is pure text/html or in parts */                     
 if (msgContent instanceof Multipart) {

     Multipart multipart = (Multipart) msgContent;


     for (int j = 0; j < multipart.getCount(); j++) {

      BodyPart bodyPart = multipart.getBodyPart(j);

      String disposition = bodyPart.getDisposition();

      if (disposition != null && (disposition.equalsIgnoreCase("ATTACHMENT"))) { 
          System.out.println("Mail have some attachment");

          DataHandler handler = bodyPart.getDataHandler();
          System.out.println("file name : " + handler.getName());                                 
        }
      else { 
              content = getText(bodyPart);  // the changed code         
        }
    }
 }
 else                
     content= ((MimeMessage)message.getContent()).toString();